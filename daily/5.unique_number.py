def array(lis):
    
    if len(lis)%2 != 0:  
        ans = lis[0]
        for i in range(1,len(lis)):
            ans ^= lis[i]
        return ans

array([2,2,3,1,1])
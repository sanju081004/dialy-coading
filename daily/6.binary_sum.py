def add_binary_nums(input1, input2): 
        max_len = max(len(input1), len(input2)) 
  
        input1 = input1.zfill(max_len) 
        input2 = input2.zfill(max_len) 
        
        result = '' 
           
        carry = 0
  
        for i in range(max_len - 1, -1, -1): 
            r = carry 
            if input1[i] == '1':
                r += 1
            else:
                r += 0
            if input2[i] == '1':
                r += 1 
            else:
                r += 0
            result = ('1' if r % 2 == 1 else '0') + result 
            if r < 2:
                carry = 0 
            else:
                carry = 1
          
        if carry !=0 :
            result = '1' + result 
  
        return result.zfill(max_len) 

add_binary_nums('0101', '1101')
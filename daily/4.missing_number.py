def missing(lis):
    m = len(lis)    
    miss_num = (m *(m + 1)//2) - sum(lis)
    return miss_num

missing([9,6,4,2,3,5,7,0,1])